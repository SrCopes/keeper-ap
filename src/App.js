import React, { useState } from "react";

import "./styles/style.css";
import Footer from "./components/Footer/Footer";
import Container from "./components/Container/Container";
import { ThemeProvider } from "styled-components";
import { GlobalStyles } from "./components/Theme/GolbalStyle";
import { lightTheme, darkTheme } from "./components/Theme/Theme";
import SettingsBrightnessIcon from "@mui/icons-material/SettingsBrightness";
import teal from "@material-ui/core/colors/teal";
import Fab from "@mui/material/Fab";
import { styled } from "@material-ui/core/styles";

function App() {
  const [theme, setTheme] = useState("light");
  const themeToggler = () => {
    theme === "light" ? setTheme("dark") : setTheme("light");
  };
  const FabButton = styled(Fab)({
    backgroundColor: teal[100],
    color: "#212121",
    width: 60,
    height: 36,
    border: 2,
    padding: "0 20px",
    "&:hover": {
      backgroundColor: teal[600],
      borderColor: teal[600],
    },
  });

  return (
    <ThemeProvider theme={theme === "light" ? lightTheme : darkTheme}>
      <>
        <GlobalStyles />
        <div>
          <header>
            <h1>Keeper</h1>
            <div className="fabButton">
              <FabButton onClick={themeToggler}>
                <SettingsBrightnessIcon />
              </FabButton>
            </div>
          </header>
          <Container />
          <Footer />
        </div>
      </>
    </ThemeProvider>
  );
}

export default App;

// inputs to create our notes
// note component
// store our notes in an array and manage them with state
// be able to delete notes
// pick a color for our notes
// edit our notes
// pinn main notes
// change the order of the notes
