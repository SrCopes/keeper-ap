import { createGlobalStyle } from "styled-components";
export const GlobalStyles = createGlobalStyle`
header{
  background: ${({ theme }) => theme.header};
  color: ${({ theme }) => theme.text};
  transition: all 0.50s linear;
}
  body {
    background: ${({ theme }) => theme.body};
    color: ${({ theme }) => theme.text};
    transition: all 0.50s linear;
    form{
        background: ${({ theme }) => theme.input};
        color: ${({ theme }) => theme.text};
        transition: all 0.50s linear;
        input{
        background: ${({ theme }) => theme.input};
        color: ${({ theme }) => theme.text};
        transition: all 0.50s linear;
      }
      textarea{
        background: ${({ theme }) => theme.input};
        color: ${({ theme }) => theme.text};
        transition: all 0.50s linear;
      } 
    }
  }
  footer{
    p{
      color: ${({ theme }) => theme.text};
      transition: all 0.50s linear;
    }   
  }
  `;
