export const lightTheme = {
  body: "#FFF",
  header: "#0c8f00",
  text: "#363537",
  input: "#FFF",
  toggleBorder: "#FFF",
  background: "#363537",
};
export const darkTheme = {
  body: "#363537",
  header: "#2F4F4F",
  text: "#b2dfdb",
  input: "#696969",
  toggleBorder: "#6B8096",
  background: "#999",
};
