import React, { useState } from "react";
import { BlockPicker } from "react-color";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import SaveIcon from "@mui/icons-material/Save";
import Fab from "@mui/material/Fab";
import { styled } from "@material-ui/core/styles";
import green from "@material-ui/core/colors/green";

export default function Note(props) {
  const [editNote, setEditNote] = useState(false);

  const [newNote, setNewNote] = useState({ title: "", content: "" });

  const [noteInput, setNoteInput] = useState({ title: "", content: "" });

  const [color, setColor] = useState("#FFFFFF");

  const FabButton = styled(Fab)({
    backgroundColor: green[600],
    color: "#212121",
    width: 60,
    height: 36,
    border: 2,
    boxShadow: "0 3px 5px 2px rgba(47, 13, 33, .7)",
    borderRadius: 3,
    padding: "0 20px",
    "&:hover": {
      backgroundColor: green[200],
      borderColor: green[200],
    },
  });

  const handleDelete = () => {
    props.deleteNote(props.id);
  };

  const handleEdit = () => {
    setEditNote(true);
  };

  const handleChangeTitle = (event) => {
    setNoteInput((prevValue) => ({
      ...prevValue,
      title: event.target.value,
    }));
  };

  const handleChangeContent = (event) => {
    setNoteInput((prevValue) => ({
      ...prevValue,
      content: event.target.value,
    }));
  };

  const handleSaveColor = (color) => {
    setColor(color.hex);
  };

  const handleSave = () => {
    setNewNote({ title: noteInput.title, content: noteInput.content });
    setEditNote(false);
  };

  return (
    <div
      className="note"
      style={{
        backgroundColor: color,
        transition: "ease all 500ms",
      }}
    >
      <h1>{newNote.title !== "" ? newNote.title : props.title}</h1>
      <p>{newNote.content !== "" ? newNote.content : props.content}</p>
      {editNote ? (
        <div>
          <input onChange={handleChangeTitle} value={noteInput.title} />
          <input onChange={handleChangeContent} value={noteInput.content} />
          <FabButton onClick={handleSave}>
            <SaveIcon />
          </FabButton>
          <BlockPicker color={color} onChangeComplete={handleSaveColor} />
        </div>
      ) : null}
      <FabButton onClick={handleDelete}>
        <DeleteIcon />
      </FabButton>
      <FabButton onClick={handleEdit}>
        <EditIcon />
      </FabButton>
    </div>
  );
}
