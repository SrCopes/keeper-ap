import React, { useState } from "react";
import Zoom from "@mui/material/Zoom";
import Fab from "@mui/material/Fab";
import AddIcon from "@mui/icons-material/Add";
import { styled } from "@material-ui/core/styles";
import green from "@material-ui/core/colors/green";

export default function CreateArea(props) {
  const [noteText, setNoteText] = useState({
    title: "",
    content: "",
  });

  const FabButton = styled(Fab)({
    backgroundColor: green[600],
    color: "#212121",
    width: 60,
    height: 36,
    border: 0,
    borderRadius: 3,
    padding: "0 10px",
    "&:hover": {
      backgroundColor: green[400],
      borderColor: green[200],
    },
  });

  const [expandedNote, setExpandedNote] = useState(false);

  const handleChange = (event) => {
    const { name, value } = event.target;

    setNoteText((prevValue) => ({
      ...prevValue,
      [name]: value,
    }));
  };

  const handleClick = (event) => {
    event.preventDefault();

    props.addNote(noteText);
    setNoteText({
      title: "",
      content: "",
    });
  };

  const { title, content } = noteText;

  return (
    <div>
      <form>
        {expandedNote && (
          <input
            type="text"
            name="title"
            value={title}
            onChange={handleChange}
            placeholder="Title"
          />
        )}
        <textarea
          name="content"
          value={content}
          row={expandedNote ? "6" : "1"}
          placeholder="Take a note..."
          onChange={handleChange}
          onClick={() => setExpandedNote(true)}
        />
        <Zoom in={expandedNote}>
          <FabButton onClick={handleClick}>
            <AddIcon />
          </FabButton>
        </Zoom>
      </form>
    </div>
  );
}
